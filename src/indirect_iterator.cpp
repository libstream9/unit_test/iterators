#include <stream9/iterators/indirect_iterator.hpp>

#include <memory>
#include <vector>

#include <boost/test/unit_test.hpp>

namespace testing {

using stream9::iterators::indirect_iterator;

BOOST_AUTO_TEST_SUITE(indirect_iterator_)

    BOOST_AUTO_TEST_CASE(step_1_)
    {
        std::vector<std::unique_ptr<int>> v;
        v.push_back(std::make_unique<int>(1));
        v.push_back(std::make_unique<int>(2));
        v.push_back(std::make_unique<int>(3));

        indirect_iterator i1 { v.begin() };

        BOOST_TEST(*i1 == 1);
        ++i1;
        BOOST_TEST(*i1 == 2);
        ++i1;
        BOOST_TEST(*i1 == 3);

        static_assert(noexcept(indirect_iterator(v.begin())));
        static_assert(noexcept(*i1));
        static_assert(noexcept(++i1));
        static_assert(noexcept(--i1));
        static_assert(noexcept(i1 + 1));
        static_assert(noexcept(i1 - i1));
        static_assert(noexcept(i1 == i1));
        static_assert(noexcept(i1 < i1));
    }

BOOST_AUTO_TEST_SUITE_END() // indirect_iterator_

} // namespace testing
