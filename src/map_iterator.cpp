#include <stream9/iterators/map_iterator.hpp>

#include <boost/test/unit_test.hpp>

#include <string>

#include <stream9/array.hpp>

namespace testing {

using stream9::map_iterator;
using stream9::array;
using std::string;

BOOST_AUTO_TEST_SUITE(map_iterator_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = array<string>;
        using I = map_iterator<T::iterator, decltype(&string::size)>;

        static_assert(std::random_access_iterator<I>);
    }

    BOOST_AUTO_TEST_CASE(step_1_)
    {
        array<string> v {
            "foo", "12345", "",
        };

        map_iterator i1 { v.begin(), &string::size };

        BOOST_TEST(*i1 == 3);
        ++i1;
        BOOST_TEST(*i1 == 5);
        ++i1;
        BOOST_TEST(*i1 == 0);

    }

    BOOST_AUTO_TEST_CASE(interaction_with_base_iterator_)
    {
        using T = array<string>;
        using I = map_iterator<T::iterator, decltype(&string::size)>;

        static_assert(requires (I i1, T::iterator i2) { i1 == i2; });
        static_assert(requires (I i1, T::iterator i2) { i1 < i2; });
        static_assert(requires (I i1, T::iterator i2) { i1 - i2; });
        static_assert(requires (I i1, T::iterator i2) { i2 - i1; });
    }

    BOOST_AUTO_TEST_CASE(noexcept_)
    {
        using T = array<string>;
        using I = map_iterator<T::iterator, decltype(&string::size)>;

        I i1;

        static_assert(noexcept(I(T().begin(), &string::size)));
        static_assert(noexcept(*i1));
        static_assert(noexcept(++i1));
        static_assert(noexcept(--i1));
        static_assert(noexcept(i1 + 1));
        static_assert(noexcept(i1 - i1));
        static_assert(noexcept(i1 == i1));
        static_assert(noexcept(i1 < i1));
    }

BOOST_AUTO_TEST_SUITE_END() // map_iterator_

} // namespace testing
