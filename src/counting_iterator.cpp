#include <stream9/iterators/counting_iterator.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(counting_iterator_)

BOOST_AUTO_TEST_CASE(concept_)
{
    using T = iter::counting_iterator<int>;

    static_assert(std::random_access_iterator<T>);
    static_assert(std::same_as<std::iter_value_t<T>, int>);
    static_assert(std::same_as<std::iter_reference_t<T>, int>);
    static_assert(std::same_as<std::iter_difference_t<T>, int>);
}

BOOST_AUTO_TEST_CASE(default_construction_)
{
    iter::counting_iterator<int> i;

    BOOST_TEST(*i == 0);
}

BOOST_AUTO_TEST_CASE(construct_with_value_)
{
    iter::counting_iterator i = 5;

    BOOST_TEST(*i == 5);
}

BOOST_AUTO_TEST_CASE(counting_)
{
    iter::counting_iterator it = 1;
    iter::counting_iterator end = 10 + 1;

    int sum = 0;
    for (; it != end; ++it) {
        sum += *it;
    }

    BOOST_TEST(sum == 55);
}

BOOST_AUTO_TEST_CASE(default_sentinel_)
{
    iter::counting_iterator i = 5;

    BOOST_CHECK(i != std::default_sentinel);
    BOOST_CHECK(i < std::default_sentinel);
}

BOOST_AUTO_TEST_SUITE_END() // counting_iterator_

} // namespace testing
