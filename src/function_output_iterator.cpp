#include <stream9/iterators/function_output_iterator.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(function_output_iterator_)

    BOOST_AUTO_TEST_CASE(value_)
    {
        json::array a;

        st9::function_output_iterator it {
            [&](auto v) { a.push_back(v); }
        };

        using T = decltype(it);
        static_assert(std::output_iterator<T, int>);

        *it++ = 1;

        int i1 = 2;
        *it++ = i1;

        *it++ = "foo";

        json::array expected { 1, 2, "foo" };
        BOOST_TEST(a == expected);
    }

    BOOST_AUTO_TEST_CASE(reference_)
    {
        json::array a;

        st9::function_output_iterator it {
            [&](auto& v) { a.push_back(v); }
        };

        using T = decltype(it);
        static_assert(std::output_iterator<T, int&>);

        int i1 = 2;
        *it++ = i1;

        json::array expected { 2 };
        BOOST_TEST(a == expected);
    }

    BOOST_AUTO_TEST_CASE(const_reference_)
    {
        json::array a;

        st9::function_output_iterator it {
            [&](auto const& v) { a.push_back(v); }
        };

        using T = decltype(it);
        static_assert(std::output_iterator<T, int const&>);

        int i1 = 2;
        *it++ = i1;

        json::array expected { 2 };
        BOOST_TEST(a == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // function_output_iterator_

} // namespace testing
