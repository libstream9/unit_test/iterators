#ifndef STREAM9_ITERATORS_TEST_SRC_NAMESPACE_HPP
#define STREAM9_ITERATORS_TEST_SRC_NAMESPACE_HPP

namespace std::ranges {}
namespace std::ranges::views {}
namespace stream9::iterators {}
namespace stream9::json {}

namespace testing {

namespace st9 { using namespace stream9; }
namespace rng { using namespace std::ranges; }
namespace rng::views { using namespace std::ranges::views; }
namespace iter { using namespace st9::iterators; }
namespace json { using namespace st9::json; }

} // namespace testing

#endif // STREAM9_ITERATORS_TEST_SRC_NAMESPACE_HPP
