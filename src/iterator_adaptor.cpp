#include <stream9/iterators/move_iterator.hpp>
#include <stream9/iterators/map_iterator.hpp>

#include <stream9/array.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(iterator_adaptor_)

    int foo(int i) { return i*i; }

    BOOST_AUTO_TEST_CASE(equality_and_ordering_)
    {
        using stream9::array;
        using stream9::map_iterator;
        using stream9::move_iterator;

        array<int> a1 {1, 2};
        array<int> a2;

        map_iterator i1 { a1.begin(), &foo };
        move_iterator i2 { i1 };

        BOOST_CHECK(i1 == i1);
        BOOST_CHECK(i2 == i2);
        BOOST_CHECK(i1 == i2);
        BOOST_CHECK(i2 == i1);

        ++i1;
        BOOST_CHECK(!(i1 < i1));
        BOOST_CHECK(!(i2 < i2));
        BOOST_CHECK(!(i1 < i2));
        BOOST_CHECK(i2 < i1);
    }

BOOST_AUTO_TEST_SUITE_END() // iterator_adaptor_

} // namespace testing
