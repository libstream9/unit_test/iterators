#include <stream9/iterators/zip_iterator.hpp>

#include "namespace.hpp"

#include <ranges>
#include <string>
#include <vector>

#include <boost/test/unit_test.hpp>

namespace testing {

static boost::test_tools::per_element per_element;

BOOST_AUTO_TEST_SUITE(zip_iterator_)

template<typename... Rs>
auto
make_zip_range(Rs&&... rs)
{
    return rng::subrange(
        iter::zip_iterator(rng::begin(std::forward<Rs>(rs))...),
        iter::zip_iterator(rng::end(std::forward<Rs>(rs))...)
    );
}

BOOST_AUTO_TEST_CASE(basic_)
{
    std::vector<int> t1 { 1, 2, 3, 4, };
    std::string t2 = "abc";

    auto r = make_zip_range(t1, t2);

    auto const expected = {
        std::make_tuple(1, 'a'), { 2, 'b' }, { 3, 'c' },
    };

    BOOST_TEST(r == expected, per_element);
}

BOOST_AUTO_TEST_CASE(increment_and_equal_)
{
    std::vector<int> t1 { 1, 2, 3, 4, };
    std::string t2 = "abc";

    auto r = make_zip_range(t1, t2);

    auto it = r.begin();
    auto end = r.end();

    BOOST_CHECK(it != end);

    ++it;
    BOOST_CHECK(it != end);

    ++it;
    BOOST_CHECK(it != end);

    ++it;
    BOOST_CHECK(it == end);
}

BOOST_AUTO_TEST_CASE(decrement_and_equal_)
{
    std::vector<int> t1 { 1, 2, 3, 4, };
    std::string t2 = "abc";

    auto r = make_zip_range(t1, t2);

    auto begin = r.begin();
    auto it = r.end();

    BOOST_CHECK(it != begin);

    --it;
    BOOST_CHECK(it != begin);

    --it;
    BOOST_CHECK(it != begin);

    --it;
    BOOST_CHECK(it == begin);
}

BOOST_AUTO_TEST_CASE(non_iterator_sentinel_)
{
    std::string t1 = "abc";
    auto t2 = rng::views::iota(0);

    static_assert(std::random_access_iterator<decltype(t2.begin())>);
    static_assert(std::same_as<decltype(t2.end()), std::unreachable_sentinel_t>);
    static_assert(!std::input_iterator<std::unreachable_sentinel_t>);

    iter::zip_iterator it { t1.begin(), t2.begin() };
    iter::zip_iterator end { t1.end(), t2.end() };

    //static_assert(std::random_access_iterator<decltype(it)>); //XXX iota_view<int> is input_iterator. WHY?
    static_assert(std::input_iterator<decltype(it)>);
    static_assert(std::input_iterator<decltype(end)>);

    BOOST_CHECK(it != end);
    BOOST_TEST(*it == std::make_tuple('a', 0));

    ++it;
    BOOST_CHECK(it != end);
    BOOST_TEST(*it == std::make_tuple('b', 1));

    ++it;
    BOOST_CHECK(it != end);
    BOOST_TEST(*it == std::make_tuple('c', 2));

    ++it;
    BOOST_CHECK(it == end);
}

BOOST_AUTO_TEST_CASE(structured_binding_)
{
    std::string t1 = "abc";
    auto t2 = rng::views::iota(0);

    iter::zip_iterator it { t1.begin(), t2.begin() };

    auto& [it1, it2] = it;

    BOOST_TEST(*it1 == 'a');
    BOOST_TEST(*it2 == 0);

    iter::zip_iterator end { t1.end(), t2.end() };

    auto& [end1, end2] = end;
    BOOST_CHECK(it1 != end1);
    BOOST_CHECK(it2 != end2);
}

BOOST_AUTO_TEST_SUITE_END() // zip_iterator_

} // namespace testing

namespace std { //XXX yeah, it's illegal, I know

template<typename... Ts>
std::ostream&
operator<<(std::ostream& os, std::tuple<Ts...> const& v)
{
    auto fn = [] <size_t... I>
        (auto& os, auto&& v, std::index_sequence<I...>) {
            ((os << (I == 0 ? "" : ", ") << std::get<I>(v)), ...);
        };

    os << '(';
    fn(os, v, std::index_sequence_for<Ts...>());
    os << ')';

    return os;
}

} // namespace std

