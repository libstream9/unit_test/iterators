#include <stream9/iterators/move_iterator.hpp>

#include <boost/test/unit_test.hpp>

#include <vector>
#include <string>

#include <stream9/json.hpp>

namespace testing {

using stream9::move_iterator;
using std::string;
using std::vector;

namespace json = stream9::json;

BOOST_AUTO_TEST_SUITE(move_iterator_)

    BOOST_AUTO_TEST_CASE(type_traits_)
    {
        using T = move_iterator<vector<string>::iterator>;

        static_assert(std::same_as<T::iterator_category, std::random_access_iterator_tag>);
    }

    BOOST_AUTO_TEST_CASE(step_1_)
    {
        vector<string> v1 { "foo", "bar" };

        vector<string> v2 {
            move_iterator(v1.begin()),
            move_iterator(v1.end()),
        };

        BOOST_TEST(v1.size() == 2);
        BOOST_TEST(v1[0].empty());
        BOOST_TEST(v1[1].empty());
        BOOST_TEST(v2.size() == 2);
        BOOST_TEST(v2[0] == "foo");
        BOOST_TEST(v2[1] == "bar");
    }

BOOST_AUTO_TEST_SUITE_END() // move_iterator_

} // namespace testing
