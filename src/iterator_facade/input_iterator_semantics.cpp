BOOST_AUTO_TEST_SUITE(input_iterator_)

    BOOST_AUTO_TEST_CASE(dereference_)
    {
        my_iterator i = "foo";

        BOOST_TEST(*i == 'f');
    }

    BOOST_AUTO_TEST_CASE(increment_advance_iterator_)
    {
        my_iterator i = "abc";

        ++i;
        BOOST_TEST(*i == 'b');

        i++;
        BOOST_TEST(*i == 'c');
    }

    BOOST_AUTO_TEST_CASE(increment_doesnt_change_iterators_address_)
    {
        my_iterator i = "abc";

        BOOST_TEST((std::addressof(++i) == std::addressof(i)));
    }

BOOST_AUTO_TEST_SUITE_END() // input_iterator_
